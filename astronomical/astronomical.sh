#!/bin/bash

# Launch with ./astronomical.sh dir_name num_threads [offset]

if (( $# > 0 ));
then
 if  ls "$1" 1>/dev/null 2>&1;
 then
    rm -r "shifted_$1" 1>/dev/null 2>&1;
    mkdir -p "shifted_$1" 1>/dev/null 2>&1;
    if (( $# > 2 ));
      then
	OMP_NUM_THREADS=$2 ./omp-astronomical "$1" `ls "$1" | grep ".bmp" -c` $3 $4 $5 $6
      else
	OMP_NUM_THREADS=$2 ./omp-astronomical "$1" `ls "$1" | grep ".bmp" -c`
      fi;
 else
    echo "Directory "$1" doesn't exist"
    exit 1;
 fi;
else
  echo "Directory name missing"
  exit 2;
fi;

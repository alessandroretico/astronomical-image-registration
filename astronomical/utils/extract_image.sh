# Extracs images from a movie.
# FFMPEG must be installed.
# Launch with ./extract_image.sh movie_name out_dir images_template_name num_images (for each seconds) [wxh]

mkdir -p $2
if (( $# > 4 ));
        then ffmpeg -i "$1" -s $5 -r $4 "$2"/"$3"%d.bmp;
else
        ffmpeg -i "$1" -r $4 "$2"/"$3"%d.bmp;
fi;

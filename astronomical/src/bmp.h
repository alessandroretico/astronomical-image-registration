/****************************************************************************
 *
 * bmp.h - Allow to manage BMP images.
 *
 * Copyright (C) 2019 Alessandro Retico <alessandro.retico(at)studio.unibo.it>
 * Last updated on 2019-09-21
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef __BMP_H__
#define __BMP_H__

#define HEADER_LEN 54
#define WIDTH_OFF 18
#define HEIGHT_OFF 22
#define NBIT_OFF 28
#define DATA_OFF 10

struct BMPimage {
  int width;
  int height;
  int size;
  int width_byte;
  int width_padding;
  int y_inc;
  short n_bit;
  int n_byte;
  int offset;
  int other_len;
  unsigned char *header;
  unsigned char *other_info;
  unsigned char *data;
};

typedef struct BMPimage BMPimage;


/**
 * Scrive un'immagine BMP in un file.
 */
void writeBMP(const BMPimage image, const char* filename);

/**
 * Libera la memoria allocata per un immagine BMP.
 */
void freeBMP(const BMPimage image);

/**
 * Stampa le informazioni di un'immagine BMP.
 */
void detailBMP(const BMPimage image);

/**
 * Restituisce l'immagine BMP contenuta in un file.
 */
BMPimage readBMP(const char* filename);

/**
 * Restituisce la copia di un immagine.
 */
BMPimage copyBMP(const BMPimage image);

/**
 * Obtain the grayscale data array of an RGB image.
 */
unsigned char* getGrayscaleData(const BMPimage image);

#endif

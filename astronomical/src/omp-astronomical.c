/****************************************************************************
 *
 * omp-astronomical.c - Astronomical image registration and reduction.
 *
 * Copyright (C) 2019 Alessandro Retico <alessandro.retico(at)studio.unibo.it>
 * Last updated on 2019-09-21
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * --------------------------------------------------------------------------
 *
 * Per eseguire il programma si puo' usare la riga di comando seguente:
 *
 * OMP_NUM_THREADS=4 ./omp-astronomical dir_name num_images [-o offset -i referenceimage_name] > out.txt
 *
 * Il primo parametro indica il numero di threads da lanciare, il secondo il
 * nome della directory che contiene le immagini (in formato .bmp), il terzo
 * il numero di immagini da utilizzare. Opzionalmente è possibile passare gli argomenti -o offset
 * (di default è pari al 5% dei pixel lungo la dimensione massima delle immagini) e -i referenceimage_name 
 * per scegliere quale immagine usare come immagine di reference. Tali parametri devono essere specificati dopo quelli 
 * obbligatori, in qulsiasi ordine.
 * L'output consiste in un'immagine ('RESULT_IMAGE_NAME') i cui pixel sono stati
 * calcolati come media della somma dei pixel corrispondenti di tutte le immagini
 * allineate. Per salvare su file le informazioni riguardanti gli allineamenti
 * delle singole immagini, reindirizzare lo standard input in un file
 * ( > nomefile).
 *
 * Per il corretto funzionamento del programma è necessario che:
 *
 * - tutte le immagini siano contenute nella stessa cartella, con estensione .bmp,
 *   rappresentazione a 24 bit, con la stessa risoluzione, lettura dal basso verso
 *   l'alto e da sinistra verso destra;
 * - sia predisposta una cartella con lo stesso percorso, ma preceduto da "shifted_",
 *   nella quale verranno salvate le immagini allineate.
 *
 ****************************************************************************/

#include "hpc.h"
#include "bmp.h"
#include "dir.h"
#include "image-registration.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define RESULT_IMAGE_NAME "result.bmp" /* Nome del file in cui scrivere l'immagine risultato. */
#define PREFIX_SHIFT_DIR "shifted_" /* Prefisso della cartella in cui salvare le immagini traslate. */
#define CL_OFF "-o" /* Argomento da far precedere al numero dell'offset. */
#define CL_IMG "-i" /* Argomento da far precedere al nome dell'immagine su cui allineare. */


int main(int argc, char* argv[]) {

  if(argc < 3) { /* Non sono stati passati tutti gli argomenti necessari */
    printf("Launch with ./omp-astronomical dir_name num_images [-o offset -i referenceimage_name]\n");
    return 1;
  }

  const char* dir_name = argv[1];
  int num_images = atoi(argv[2]);
  
   /* Parametri opzionali. */
  int offset = 0;
  char* reference_filename = NULL;
  if (argc > 4){ /* Sono stati passati parametri opzionali. */
	/* Offset. */
    if(!strncmp(argv[3], CL_OFF, strlen(CL_OFF))) offset = atoi(argv[4]);
    else if(argc > 6 && !strncmp(argv[5], CL_OFF, strlen(CL_OFF))) offset = atoi(argv[6]);
    /* Referenceimage_name. */
    if(!strncmp(argv[3], CL_IMG, strlen(CL_IMG))) reference_filename = argv[4];
    else if(argc > 6 && !strncmp(argv[5], CL_IMG, strlen(CL_IMG))) reference_filename = argv[6];
  }
  /* Nomi delle immagini da analizzare. */
  char** files_name = (char**)malloc(sizeof(char*) * num_images);

  /* Salva il numero effettivo di immagini da analizzare. */
  num_images = getBMPnames(dir_name, files_name, num_images, reference_filename);

  /* Nomi delle immagini allineate (-1 perchè quella reference non verrà traslata). */
  char** shifted_files_name = (char**)malloc(sizeof(char*) * (num_images - 1));
  
  /* 
   * Controlla se ci sono meno di due immagini nella cartella (non ha senso eseguire la computazione) o se si 
   * sono verificati errori: in tal caso l'esecuzione viene interrotta.
   */
  if(num_images < 2) {
    printf("Less than 2 images are available or something where wrong during images' reading\n");
    return 2;
  }

  /* La prima immagine viene selezionata come immagine di reference (su cui allineare le altre). */
  const BMPimage reference_image = readBMP(files_name[0]);
  unsigned char* reference_image_g = getGrayscaleData(reference_image); /* Si ottengono i pixel grayscale dell'immagine di reference. */
  
  if(offset == 0) { /* Non è stato passato il parametro relativo all'offset: ne viene calcolato uno di default in base alle dimensioni dell'immagine. */
    const int max_dim = reference_image.width > reference_image.height ? reference_image.width : reference_image.height;
    offset =  max_dim * 5 / 100; /* Per default si calcola come il 5% dei pixel lungo la dimensione maggiore dell'immagine. */ 
  }

  /* Numero di confronti da effettuare per ogni immagine. */
  const int num_offsets = (offset * 2 + 1) * (offset * 2 + 1);
  /* Precalcola gli offsets da applicare per allineare le immagini. */
  offsetInfo* offsets = prepareOffsets(reference_image.width, reference_image.height, offset);
  
  fprintf(stderr, "\nAligning %d images with %d offset and %s as reference image\n\n", num_images - 1, offset, files_name[0]);

  const double start = hpc_gettime();

  for(int file_idx = 1; file_idx < num_images; file_idx++) { /* Per ogni immagine da allineare. */

    char* filename = files_name[file_idx]; /* Nome del file da computare. */
    const BMPimage target_image = readBMP(filename); /* Immagine target (da allineare). */
    unsigned char* target_image_g = getGrayscaleData(target_image); /* Si ottengono i pixel grayscale dell'immagine target. */

    /* Cerca l'offset dell'allineamento migliore (sui pixel grayscale delle immagini). */
    alignInfo best_align = alignBMP(target_image_g, reference_image_g,  reference_image.width, offsets, num_offsets);
    
	fprintf(stderr, "\r%d/%d allineated ", file_idx, num_images - 1 ); 
    printf("%s allineated with [%d, %d] offset and %ld differences\n",filename, best_align.x_off, best_align.y_off, best_align.diff);
	
    /* Esegue la traslazione (sulle immagini in RGB non sui pixel grayscale). */
    const BMPimage curr_image = shiftBMP(target_image, reference_image, best_align.x_off, best_align.y_off);
   
    /* Salva il nome del file. */
    const int len = strlen(PREFIX_SHIFT_DIR) + strlen(filename) + 1;
    shifted_files_name[file_idx - 1] = (char*)malloc(sizeof(char)*len);
    sprintf(shifted_files_name[file_idx - 1], "%s%s", PREFIX_SHIFT_DIR, filename);
    writeBMP(curr_image, shifted_files_name[file_idx - 1]); /* Salva l'immagine su file. */
    
    /* Libera la memoria. */
    freeBMP(target_image);
    free(target_image_g); 
    freeBMP(curr_image);
  }
  
   /* Libera la memoria. */
   free(offsets);
   for(int i = 0; i < num_images; i++) free(files_name[i]);
   free(files_name);
  
   fprintf(stderr,"\n\nReduction started ... ");

  /*
   * Esegue la riduzione sull'immagine di reference, usando le 'num_images' - 1 (- 1, dato che 'num_images' comprende anche l'immagine
   * di reference) immagini allineate.
   */
  BMPimage result_image = reductBMP(reference_image, shifted_files_name, num_images - 1);
  fprintf(stderr,"\rReduction complete     \n");
  writeBMP(result_image, RESULT_IMAGE_NAME); /* Scrive l'immagine risultato su file. */
  
  const double stop = hpc_gettime();
   
  /* Libera la memoria. */
  freeBMP(reference_image);
  for(int i = 0; i < num_images - 1; i++) free(shifted_files_name[i]);
  free(shifted_files_name);
  freeBMP(result_image);
  free(reference_image_g);

 
  fprintf(stderr, "\nTime: %f\n", stop - start);

  return 0;
}

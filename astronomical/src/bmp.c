/****************************************************************************
 *
 * bmp.c - Allow to manage BMP images.
 *
 * Copyright (C) 2019 Alessandro Retico <alessandro.retico(at)studio.unibo.it>
 * Last updated on 2019-09-21
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void writeBMP(const BMPimage image, const char* filename) {

  FILE* f = fopen(filename, "wb");

  for(int i = 0; i < HEADER_LEN; i++) fprintf(f,"%c", image.header[i]); /* Scrive l'header. */
  for(int i = 0; i < image.other_len ; i++) fprintf(f,"%c", image.other_info[i]); /* Scrive le altre informazioni. */
  for(int i = 0; i < image.size; i++) fprintf(f,"%c", image.data[i]); /* Scrive il valore dei pixel dell'immagine. */

  fclose(f);
}

void freeBMP(const BMPimage image) {

  free(image.data);
  free(image.header);
  free(image.other_info);
}

void detailBMP(const BMPimage image) {

  printf("Dimension: %d x %d\n", image.width, image.height);
  printf("Byte per Pixel: %d\n", image.n_byte);
  printf("Width padding: %d\n", image.width_padding);
  printf("Width lenght: %d\n", image.y_inc);
  printf("Total size : %d bytes\n", (image.offset + image.size));
}

BMPimage readBMP(const char* filename) {

  FILE* f = fopen(filename, "rb");
  BMPimage image;

  image.header = (unsigned char*)malloc(sizeof(unsigned char)*HEADER_LEN); /* Alloca lo spazio per l'header. */
  size_t header_len = fread(image.header, sizeof(unsigned char), HEADER_LEN, f); /* Scrive l'header (54 byte) nella memoria appena allocata. */
  if(header_len < HEADER_LEN) printf("Error in writing image's other info\n");
  
  /* Estrae alucune informazioni di interesse dall'header. */
  memcpy(&(image.width), image.header + WIDTH_OFF, sizeof(int));    /* Lunghezza. 		          */
  memcpy(&(image.height), image.header + HEIGHT_OFF, sizeof(int));  /* Altezza.   		          */
  memcpy(&(image.n_bit), image.header + NBIT_OFF, sizeof(short));   /* Numero di bit per pixel.   */
  memcpy(&(image.offset), image.header + DATA_OFF, sizeof(int));    /* Offset inizio pixel.       */

  /* L'altezza dell'immagine potrebbe essere espressa con segno negativo ad indicare la direzione di lettura. */
  image.height = abs(image.height);
  image.other_len = image.offset - HEADER_LEN;

  image.other_info = (unsigned char*)malloc(image.other_len * sizeof(unsigned char)); /* Alloca lo spazio per le altre info. */
  size_t len_other_info = fread(image.other_info, sizeof(unsigned char), image.other_len, f); /* Scrive le altre info nella memoria appena allocata. */
  if(len_other_info < image.other_len) printf("Error in writing image's other info\n");
  
  image.n_byte = image.n_bit / 8;
  image.width_byte = image.width * image.n_byte;
  image.width_padding = image.width_byte % 4; /* Ogni linea di un'immagine deve essere multipla di 4 byte. */
  image.y_inc = image.width_byte + image.width_padding;
  image.size = image.y_inc * image.height;
  image.data = (unsigned char*)malloc(sizeof(unsigned char) * image.size); /* Alloca il giusto numero di bit per ogni pixel. */
  size_t len_data = fread(image.data, sizeof(unsigned char), image.size, f); /* Scrive i pixel dell'immagine nella memoria appena allocata. */
  if(len_data < image.size) printf("Error in writing image's data\n");
  
  fclose(f);

  return image;
}

BMPimage copyBMP(const BMPimage image) {

  BMPimage copy = image;
  copy.header = (unsigned char*)malloc(sizeof(unsigned char) * HEADER_LEN);
  copy.other_info = (unsigned char*)malloc(sizeof(unsigned char) * copy.other_len);
  copy.data = (unsigned char*)malloc(sizeof(unsigned char) * copy.size);
  memcpy(copy.header, image.header, sizeof(unsigned char) * HEADER_LEN);
  memcpy(copy.other_info, image.other_info, sizeof(unsigned char) * copy.other_len);
  memcpy(copy.data, image.data, sizeof(unsigned char) * copy.size);

  return copy;
}

unsigned char* getGrayscaleData(const BMPimage image){
  int size = image.width * image.height; /* Un solo byte per pixel. */
  unsigned char* g_image = (unsigned char *) malloc (sizeof(unsigned char) * size);

  int pos = 0;
  for(int y = 0; y < image.height; y++){
	const int y_val = image.y_inc * y;
    for(int x = 0; x < image.width_byte; x+=image.n_byte){ /* Mappa il pixel RGB in un singolo pixel in grayscale. */
      unsigned char lum = image.data[y_val + x] * 0.11 + image.data[y_val + x + 1] * 0.59 + image.data[y_val + x + 2] * 0.3;
      g_image[pos++] = lum;
    }
  }
    
  return g_image;
}

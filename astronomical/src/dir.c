/****************************************************************************
 *
 * dir.c - Allow to explore directories.
 *
 * Copyright (C) 2019 Alessandro Retico <alessandro.retico(at)studio.unibo.it>
 * Last updated on 2019-09-21
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "dir.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <string.h>

int search_file(const char* dir_name, const char* filename){
	const int filename_len = strlen(filename);
	int pos = 0; /* Posizione del file attuale. */
	DIR *d;
    struct dirent *dir;
    d = opendir(dir_name);
    if(d){ 
	  while((dir = readdir(d))) {
		if(!strncmp(dir->d_name, filename, filename_len)) { /* Trovato. */
			closedir(d);
			return pos; /* Ritorna la posizione. */
		}
		pos++; /* Incrementa la posizione del file attuale. */
	  }
	  closedir(d);
	} else printf("Directory %s doesn't exists\n", dir_name);
	return -1; /* Non è stato trovato. */
}

int isBMPfile (const char * file) {

  size_t file_len = strlen(file);

  /*
   * Se il nome del file passato ha lunghezza maggiore di 'BMP_EXT_LEN', controlla se l'estensione
   * corrisponde a quella di un immagine BMP.
   */
  if(file_len - BMP_EXT_LEN > 0) return !strncmp(file + (file_len - BMP_EXT_LEN), BMP_EXT, BMP_EXT_LEN);
  else return 0;
}

int getBMPnames(const char* dir_name, char** files_name, const int num_files, const char* reference_filename) {

  DIR *d;
  struct dirent *dir;
  const int arg_pass = reference_filename != NULL; /*  1 se il nome dell'immagine reference è stato passato, 0 altrimenti. */
  int file_readed = 0; /* Numero di nomi di immagini BMP inseriti in 'files_name'. */
  int reference_pos = -1; /* Posizione dell'immagine riferimento. */
  int pos = 0; /* Posizione del file attuale. */
  d = opendir(dir_name);
  if(d){ 
	  if(arg_pass) { /* Bisogna cercare il file. */
		reference_pos = search_file(dir_name, reference_filename); /* Controlla se il file c'è e ottiene la sua posizione. */
		if(reference_pos < 0) { /* Il file non esiste. */
			printf("File %s doesn't exists in %s\n", reference_filename, dir_name);
			return -1;
		}
		files_name[file_readed] = (char*) malloc (sizeof(char) * (strlen(dir_name) + strlen(reference_filename) + 2));
		/* Copia nella memoria appena allocata il nome e incrementa il numero di nomi inseriti. */
		sprintf(files_name[file_readed++], "%s/%s", dir_name, reference_filename);
	  }
	  while((dir = readdir(d)) != NULL && file_readed < num_files) {
		if(isBMPfile(dir->d_name) && pos != reference_pos) { /* Se il file è un immagine BMP e non è il file reference. */
		  /* Alloca lo spazio per il nome del file (cartella/nomefile). */
		  files_name[file_readed] = (char*) malloc (sizeof(char) * (strlen(dir_name) + strlen(dir->d_name) + 2));
		  /* Copia nella memoria appena allocata il nome e incrementa il numero di nomi inseriti. */
		  sprintf(files_name[file_readed++], "%s/%s", dir_name, dir->d_name);
		}
		pos++; /* Incrementa la posizione del file attuale. */
	  }
	 closedir(d);
  } else { /* La cartella non è stata aperta correttamente. */
    printf("Directory %s doesn't exists\n", dir_name);
    return -1;
  }
  
  return file_readed;
}

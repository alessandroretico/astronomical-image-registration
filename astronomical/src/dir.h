/****************************************************************************
 *
 * dir.h - Allow to explore directories.
 *
 * Copyright (C) 2019 Alessandro Retico <alessandro.retico(at)studio.unibo.it>
 * Last updated on 2019-09-21
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef __DIR_H__
#define __DIR_H__

#define BMP_EXT ".bmp"
#define BMP_EXT_LEN 4

/**
 * Restituisce 1 se il nome del file passato è un file bmp, 0 altrimenti.
 */
int isBMPfile (const char * file);

/**
 * Restituisce il numero (al massimo 'num_files') di nomi delle immagini BMP della cartella 'dir_name'
 * inseriti in 'files_name', posizionando il file 'reference_filename' come primo.
 * Nel caso in cui non sia stato possibile aprire la cartella o il file 'reference_filename' non sia presnete in 'dir_name',
 * restituisce -1.
 */
int getBMPnames(const char* dir_name, char** files_name, const int num_files, const char* reference_filename );

/**
 * Restituisce la posizione del file 'filename' all'interno della cartella 'dir_name'; nel caso in cui la cartella o il file
 * non esistessero, viene ritornato il valore -1.
 */
 int search_file(const char* dir_name, const char* filename);

#endif

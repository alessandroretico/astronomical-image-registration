/****************************************************************************
 *
 * image-registration.h - Allow to align and reduct images.
 *
 * Copyright (C) 2019 Alessandro Retico <alessandro.retico(at)studio.unibo.it>
 * Last updated on 2019-09-21
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef __IMAGE_REGISTATION_H__
#define __IMAGE_REGISTATION_H__

#include "bmp.h"

struct alignInfo {
  int x_off;
  int y_off;
  long diff;
};

typedef struct alignInfo alignInfo;

struct offsetInfo {
  int y_off;
  int y_init;
  int y_limit;
  int x_off;
  int x_init;
  int x_limit;
  int reference_off;
  int target_off;
};

typedef struct offsetInfo offsetInfo;

/**
 * Prepara i dati inerenti agli offsets per eseguire l'allineamento (da [-off, -off] a [off, off]).  
 */
offsetInfo* prepareOffsets(const int width, const int height, const int off);

/**
 * Individua il miglior allineamento di 'target_image' rispetto a 'reference_image' nel range ('-off', '-off') e 
 * ('off', 'off').
 * Per individuarlo, si confronta 'target_image' con 'reference_image' traslando 'target_image' in tutti i possibili 
 * modi all'interno del range precedentemente definito. Il confronto avviene pixel per pixel, memorizzando la 
 * somma del valore assoluto delle differenze di tali confronti. Al termine dell'algoritmo verranno restituite 
 * le informazioni riguardanti il miglior allineamento (quello con somma delle differenze minore).
 * Il parametro 'offsets' contiene le informazioni relative agli offsets, mentre 'num_offset' indica il numero 
 * di confronti da effettuare.
 */
alignInfo alignBMP(const unsigned char* target, const unsigned char* reference, const int y_inc, const offsetInfo* offsets, const int num_offset);

/**
 * Restituisce 'target_image' traslata di ('x_off', 'y_off') : i pixel per cui non si ha un valore (ad esempio con uno 
 * spostamento a destra di 3, i primi 3 a sinistra di ogni riga), sono riempiti con il valore dei pixel corrispondenti
 * in 'reference_image'.
 */
BMPimage shiftBMP(const BMPimage target_image, const BMPimage reference_image, const int x_off, const int y_off);

/**
 * Ritorna un immagine i cui pixel sono calcolati come la media della somma dei rispettivi pixel dell'immagine 
 * 'reference_image' e delle altre 'num_images' immagini (i cui nomi sono salvati in 'files_name').
 */
BMPimage reductBMP(const BMPimage reference_image, char** files_name, const int num_images);

#endif

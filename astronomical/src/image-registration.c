/****************************************************************************
 *
 * image-registration.c - Allow to align and reduct images.
 *
 * Copyright (C) 2019 Alessandro Retico <alessandro.retico(at)studio.unibo.it>
 * Last updated on 2019-09-21
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "image-registration.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <omp.h>
#include <arm_neon.h>

offsetInfo* prepareOffsets(const int width, const int height, const int off) {

  const int y_num_off = off * 2 + 1; /* Numero di offsets per riga. */
  const int num_off = y_num_off * y_num_off; /* Numero di offsets (da [-off, -off] a [off, off]). */

  /* Alloca spazio per salvare i dati di tutti gli offset. */
  offsetInfo* offsets = (offsetInfo*) malloc (sizeof(offsetInfo) * (num_off));
  
#pragma omp parallel for
  for(int y_off = -off; y_off <= off; y_off++){ /* Per ogni y di (-off, off) */

	const int curr_y_off = (y_off + off) * y_num_off; /* Parte dell'indice a cui posizionare l'offset. */
    /*
     * Indici di y rispetto a 'reference_image': quelli di 'target_image' verranno calcolati sottraendro 'y_off'
     * (positivo o negativo che sia). Dato che un file bmp viene letto dal basso verso l'alto, se l'offset è negativo,
     * 'y_init' sarà '0' e 'y_limit' sarà 'height + y_off', mentre se è positivo 'y_init' sarà 'y_off' e 'y_limit'
     * sarà 'height'.
     * Ad esempio se 'y_off' è uguale a -1, l'immagine dovrà essere spostata verso l'alto di 1, perciò la prima riga
     * ([0]) di 'reference_image' deve essere confrontata con la seconda ([0 - (-1)] = [1]) di 'target_image' e la penultima riga di
     * 'reference_image' ([(height - 1) + (-1)] = [height - 2]) con l'ultima ([(height - 2) - (-1)] = [height - 1]) di 'target_image'.
     * Se invece è uguale ad 1, l'immagine dovrà essere spostata verso il basso di 1, perciò la seconda riga 
     * ([1]) di 'reference_image' deve essere confrontata con la prima ([1 - (1)] = [0]) di 'target_image' e l'ultima riga di
     * 'reference_image'([height - 1]) con la penultima ([(height - 1) - (1) ] = [height - 2]) di 'target_image'.
     */
    const int y_init = y_off < 0 ? 0 : y_off;
    const int y_limit = y_off < 0 ? height + y_off : height;

    /* Precalcola gli offset di y. */
    const int reference_y_off = y_init * width;
    const int target_y_off = (y_init - y_off) * width;

    for(int x_off = -off; x_off <= off; x_off++){ /* Per ogni x di (-off, off) */

	  const int curr_off = curr_y_off + (x_off + off); /* Indice a cui posizionare l'offset. */
	  
      offsets[curr_off].y_off = -y_off; /* -y_off dato che un immagine BMP si legge dal basso verso l'alto (bisogna ribaltare l'asse). */
      offsets[curr_off].y_init = y_init;
      offsets[curr_off].y_limit = y_limit;
      offsets[curr_off].x_off = x_off;

      /*
       * Indici di x rispetto a 'reference_image': quelli di 'target_image' verranno calcolati sottraendo 'x_off'.
       * (positivo o negativo che sia). Dato che un file bmp viene letto da sinistra verso destra, se l'offset è negativo, 
       * 'x_init' sarà '0' e 'x_limit' sarà 'width + x_off', mentre se è positivo 'x_init' sarà 'x_off' e 'x_limit' sarà
       * 'width'.
       * Ad esempio se 'x_off' è uguale a -1, l'immagine dovrà essere spostata verso sinistra di 1, perciò la prima colonna
       * ([0]) di 'reference_image' deve essere confrontata con la seconda ([0 - (-1)] = [1]) di 'target_image' e la penultima colonna di
       * 'reference_image' ([(width - 1) + (-1)] = [width - 2]) con l'ultima ([(width - 2) - (-1)] = [width - 1]) di 'target_image'.
       * Se invece è uguale ad 1, l'immagine dovrà essere spostata verso destra di 1, perciò la seconda colonna ([1]) di 'reference_image'
       * deve essere confrontata con la prima ([1 - (1)] = [0]) di 'target_image' e l'ultima colonna di 'reference_image'([width - 1]) con
       * la penultima ([(width - 1) - (1)] = [width - 2]) di 'target_image'.
       */
      offsets[curr_off].x_init = x_off < 0 ?  0 : x_off;
      offsets[curr_off].x_limit = x_off < 0 ? width + x_off : width;

      /* Precalcola gli offset. */
      offsets[curr_off].reference_off = reference_y_off;
      offsets[curr_off].target_off = target_y_off - x_off;
    }
  }

  return offsets;
}

alignInfo alignBMP(const unsigned char* target, const unsigned char* reference, const int y_inc, const offsetInfo* offsets, const int num_offset) {
	
  const int num_threads = omp_get_max_threads();
  
  /* Ogni thread salverà il miglior allineamento calcolato in questo array. */
  alignInfo* best_align = (alignInfo *)malloc(sizeof(alignInfo) * num_threads);
 
  for(int i = 0; i < num_threads; i++) { /* Per ogni thread, inizializza le informazioni sull'allineamento. */
    best_align[i].x_off = 0;
    best_align[i].y_off = 0;
    best_align[i].diff = LONG_MAX; /* La differenza del primo allineamento calcolato sarà sicuramente minore. */
  }

#pragma omp parallel
{
   const int my_rank = omp_get_thread_num(); /* Indice del thread. */
   const int my_start = my_rank * num_offset / num_threads; /* Indice di inzio del thread. */ 
   const int my_stop = (my_rank + 1) * num_offset / num_threads; /* Indice di fine del thread. */
   
  for(int i = my_start; i < my_stop; i++){ /* Ogni thread computa il miglior allineamento su una parte degli offsets. */

    long diff = 0; /* Somma delle differenze. */

    /* Salva le informazioni (precalcolate) dell'offset attuale per poter calcolare l'allineamento. */
    const int y_init = offsets[i].y_init;
    const int y_limit = offsets[i].y_limit;
    const int x_init = offsets[i].x_init;
    const int x_limit = offsets[i].x_limit;

    int target_off = offsets[i].target_off;
    int reference_off = offsets[i].reference_off;
    int x = 0;
    for(int y = y_init; y < y_limit; y++) {
      for(x = x_init; x < x_limit - 15; x+=16) { /* Computa 16 pixel alla volta. */
		uint8x16_t target_a = vld1q_u8(target + target_off + x); /* Carica i dati dell'immagine target. */
		uint8x16_t reference_a = vld1q_u8(reference + reference_off + x); /* Carica i dati dell'immagine reference. */
		uint8x16_t result = vabdq_u8(target_a, reference_a); /* Valore assoluto della differenza dei pixel caricati. */
        for(int z = 0; z < 16; z++) diff += result[z]; /* Somma il valore assoluto delle 16 differenze eseguite. */
	  }
	  /* Esegue l'operazione sui pixel rimanenti. */
	  for(; x < x_limit; x++) diff += abs(target[target_off + x] - reference[reference_off + x]); 

      /* Aumenta li offset di una riga */
      target_off += y_inc;
      reference_off += y_inc;

      /*
       * Se la somma delle differenze attuale, è maggiore della migliore trovata fino ad ora, interrompe la computazione
       * per l'offset attuale.
       */
      if(best_align[my_rank].diff < diff) break;
    }

    /*
     * Se la somma delle differenze attuale, è minore della migliore trovata fino ad ora, salva le informazioni relative
     * all'offset attuale come il migliore.
     */
    if(diff < best_align[my_rank].diff) {
      best_align[my_rank].diff = diff;
      best_align[my_rank].x_off = offsets[i].x_off;
      best_align[my_rank].y_off = offsets[i].y_off;
    }
  }
}
  alignInfo result = best_align[0];
  
  /* Determina qual è l'offset migliore tra quelli calcolati da ogni thread. */
  for(int i = 1; i < num_threads; i++)
	if(result.diff > best_align[i].diff) result = best_align[i];
	
   free(best_align); /* Libera la memoria. */
 
  return result;
}

BMPimage shiftBMP(const BMPimage target_image, const BMPimage reference_image, const int x_off,  int y_off) {

  /* 
  * Copia nell'immagine da restituire, 'reference_image': così facendo, i pixel esclusi dalla traslazione avranno il 
  * valore corrispondente ai pixel di 'reference_image'.
  */
  const BMPimage shifted_image = copyBMP(reference_image); 
   
  const int n_byte = shifted_image.n_byte;
  const int height = shifted_image.height;
  const int width = shifted_image.width;
  
  y_off = -y_off; /* L'offset verticale viene invertito poichè l'immagine viene letta dal basso verso l'alto (bisogna ribaltare l'asse). */

  /*
   * Indici di y rispetto a 'shifted_image': quelli di 'target_image' verranno calcolati sottraendo 'y_off'
   * (positivo o negativo che sia). Dato che un file bmp viene letto dal basso verso l'alto, se l'offset è negativo,
   * 'y_init' sarà '0' e 'y_limit' sarà 'height + y_off', mentre se è positivo 'y_init' sarà 'y_off' e 'y_limit'
   * sarà 'height'.
   * Ad esempio se 'y_off' è uguale a -1, l'immagine dovrà essere spostata verso l'alto di 1, perciò la prima riga
   * ([0]) di 'shifted_image' deve essere la seconda ([0 - (-1)] = [0]) di 'target_image' e la penultima riga di
   * 'shifted_image' ([(height - 1) + (-1)] = [height - 2]) deve essere l'ultima ([(height - 2) - (-1)] = [height - 1]) di 'target_image'.
   * Se invece è uguale ad 1, l'immagine dovrà essere spostata verso il basso di 1, perciò la seconda riga
   * ([1]) di 'shifted_image' deve essere la prima ([1 - (1)] = [0]) di 'target_image' e l'ultima riga di
   * 'shifted_image'([(height - 1)]) deve essere la penultima ([(height - 1) - (1)] = [height - 2]) di 'target_image'.
   */
  const int y_init = y_off < 0 ? 0 : y_off;
  const int y_limit = y_off < 0 ? height + y_off : height;

  const int y_inc = shifted_image.y_inc; /* Lunghezza di una riga. */

  /*
   * Indici di x rispetto a 'shifted_image': quelli di 'target_image' verranno calcolati sottraendo 'x_off' 
   * (positivo o negativo che sia). Dato che un file bmp viene letto da sinistra verso destra, se l'offset è negativo, 
   * 'x_init' sarà 0 e 'x_limit' sarà '(width + x_off) * n_byte', mentre se è positivo 'x_init' sarà 'x_off * n_byte' e 'x_limit' sarà
   * 'width * n_byte'.
   * Ad esempio se 'x_off' è uguale a -1, l'immagine dovrà essere spostata verso sinistra di 1, perciò la prima colonna
   * ([0]) di 'shifted_image' deve essere la seconda ([0 - (-1)] = [1]) di 'target_image' e la penultima colonna di
   * 'shifted_image' ([(width - 1) + (-1)] = [width - 2]) deve essere l'ultima ([(width - 2) - (-1)] = [width - 1]) di 'target_image'.
   * Se invece è uguale ad 1, l'immagine dovrà essere spostata verso destra di 1, perciò la seconda colonna ([1]) di 'shifted_image'
   * deve essere la prima ([1 - (1)] = [0]) di 'target_image' e l'ultima colonna di 'shifted_image'([width - 1]) deve essere
   * la penultima ([(width - 1) - (1)] = [width - 2]) di 'target_image'.
   */
  const int x_init = x_off < 0 ? 0 : x_off * n_byte;
  const int x_limit = x_off < 0 ? (width + x_off) * n_byte: width * n_byte;
  
  /* Precalcola gli offset. */
  const int target_off = (-y_off * y_inc) + ((-x_off) * n_byte);
   
  /* Crea i thread che eseguiranno la traslazione */
 #pragma omp parallel for
  for(int y = y_init; y < y_limit; y++){
	const int y_val = y * y_inc;
    for(int x = x_init; x < x_limit; x++)
      shifted_image.data[y_val + x] = target_image.data[target_off + y_val + x];
  }
  
  return shifted_image;
}

BMPimage reductBMP(const BMPimage reference_image, char** files_name, const int num_images) {

  /* Istanzia le informazioni dell'immagini (il valore dei pixel verrà modificato). */
  const BMPimage result_image = copyBMP(reference_image); 
  const int image_len = reference_image.size;

  /* Alloca un array di interi utilizzato per calcolare la somma dei valori dei pixel di tutte le immagini. */
  int* image_sum = (int*)malloc(sizeof(int)*image_len);

  /* Inizializza il valore dall'array con il valore dei pixel di 'reference_image'. */
  #pragma omp parallel for
  for(int i = 0; i < image_len; i++) image_sum[i] = reference_image.data[i]; 

  for(int j = 0; j < num_images; j++){ /* Per tutte le immagini. */
    
    const BMPimage target_image = readBMP(files_name[j]);
    
    /* Somma il valore di ogni pixel, all'array. */
    #pragma omp parallel for 
    for(int i = 0; i < image_len; i++) image_sum[i] += target_image.data[i]; 

     /* Libera la memoria. */
     freeBMP(target_image);
  }

   /* Numero totale di immagini utilizzate per calcolare l'immagine risultato (+ 1 per includere la 'reference_image') */
   const int tot_n_images = num_images + 1;
   
  /*
   * Assegna ad ogni pixel dell'immagine risultato, la media della somma del valore dell'immagine reference e di 
   * quella delle altre immagini.
   */
  #pragma omp parallel for 
  for(int i = 0; i < image_len; i++) result_image.data[i] = image_sum[i] / tot_n_images;

  free(image_sum); /* Libera la memoria. */

  return result_image;
}


